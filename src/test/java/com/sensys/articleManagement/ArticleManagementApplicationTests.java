package com.sensys.articleManagement;

import com.sensys.articleManagement.controller.ArticleManagementController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class ArticleManagementApplicationTests {

	@Autowired
	private ArticleManagementController controller;

	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
