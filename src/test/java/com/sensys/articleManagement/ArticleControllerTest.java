package com.sensys.articleManagement;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.sensys.articleManagement.controller.requests.ArticleRequestModel;
import com.sensys.articleManagement.repository.ArticleRepository;
import com.sensys.articleManagement.repository.documents.ArticleItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ArticleControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    ObjectMapper mapper;

    @Autowired
    ArticleRepository articleRepository;

    @Test
    void postArticle() throws Exception {
        articleRepository.deleteAll();
        ArticleRequestModel request = new ArticleRequestModel("title",
                "test article description", List.of("articleId1234"));

        mockMvc.perform(post("/article/new_article")
                        .contentType("application/json")
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk());

        List<ArticleItem> articleItems = articleRepository.findArticleItemByTitle("title");
        assertThat(articleItems.size() != 0);
        assertThat(articleItems.get(0).getTitle()).isEqualTo("title");
    }

    @Test
    void getArticles() throws Exception {
        postArticle();
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/article/list_articles")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

}
