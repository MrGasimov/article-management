package com.sensys.articleManagement.mappers;

import com.sensys.articleManagement.controller.requests.ArticleRequestModel;
import com.sensys.articleManagement.controller.responses.ArticleResponseModel;
import com.sensys.articleManagement.repository.documents.ArticleItem;
import org.mapstruct.Mapper;

@Mapper
public interface ArticleMapper {
    ArticleItem mapArticleRequest(ArticleRequestModel requestModel);

    ArticleResponseModel mapArticleResponse(ArticleItem articleItem);

}
