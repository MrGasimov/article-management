package com.sensys.articleManagement.service;


import com.sensys.articleManagement.controller.requests.ArticleRequestModel;
import com.sensys.articleManagement.controller.responses.ArticleListItemResponseModel;
import com.sensys.articleManagement.controller.responses.ArticleResponseModel;
import com.sensys.articleManagement.mappers.ArticleMapper;
import com.sensys.articleManagement.repository.ArticleRepository;
import com.sensys.articleManagement.repository.documents.ArticleItem;
import com.sensys.articleManagement.service.exceptions.ArticleNotFoundException;
import com.sensys.articleManagement.service.exceptions.ImageAttachmentLimitException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ArticleService {

    private final ArticleRepository articleRepository;
    private final ArticleMapper articleMapper;
    private final ImageService imageService;

    public ArticleResponseModel createArticle(ArticleRequestModel model) {
        var articleItem = articleMapper.mapArticleRequest(model);
        return articleMapper.mapArticleResponse(articleRepository.insert(articleItem));
    }

    public List<ArticleItem> getArticleByTitle(String title){
        return articleRepository.findArticleItemByTitle(title);
    }

    public List<ArticleListItemResponseModel> listArticles() {
        var articleList = articleRepository.findAll();
        var responseList = articleList.stream().filter(articleItem -> articleItem.getAttachedImages().size() > 0)
                .map(a -> new ArticleListItemResponseModel(a.getTitle(), a.getAttachedImages().size()))
                .collect(Collectors.toList());
        return responseList;
    }

    public ArticleResponseModel addImageToArticle(MultipartFile file, String articleId) {
        var article = articleRepository.findById(articleId).orElseThrow(() -> new ArticleNotFoundException("No Article found with given id!"));

        if (article.getAttachedImages() != null && article.getAttachedImages().size() < 3) {
            String id = imageService.addImage(file);
            article.getAttachedImages().add(id);
            articleRepository.save(article);
        } else {
            throw new ImageAttachmentLimitException("More than 3 images are not allowed to attach  article!");
        }
        return articleMapper.mapArticleResponse(article);
    }
}
