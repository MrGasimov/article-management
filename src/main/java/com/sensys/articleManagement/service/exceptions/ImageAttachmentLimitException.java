package com.sensys.articleManagement.service.exceptions;


public class ImageAttachmentLimitException extends ArticleException {
    public ImageAttachmentLimitException(String message) {
        super(message);
    }

    public ImageAttachmentLimitException(String message, Throwable cause) {
        super(message, cause);
    }

}
