package com.sensys.articleManagement.service.exceptions;

public class ArticleNotFoundException extends ArticleException{
    public ArticleNotFoundException(String message) {
        super(message);
    }

    public ArticleNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
