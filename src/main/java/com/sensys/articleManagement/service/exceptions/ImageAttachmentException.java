package com.sensys.articleManagement.service.exceptions;

public class ImageAttachmentException extends RuntimeException{

    public ImageAttachmentException(String message) {
        super(message);
    }

    public ImageAttachmentException(String message, Throwable cause) {
        super(message, cause);
    }
}
