package com.sensys.articleManagement.service;


import com.sensys.articleManagement.repository.ImageRepository;
import com.sensys.articleManagement.repository.documents.Image;
import com.sensys.articleManagement.service.exceptions.ImageAttachmentException;
import lombok.RequiredArgsConstructor;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class ImageService {

    private final ImageRepository imageRepository;

    public String addImage(MultipartFile file) {
        Image image = new Image();
        try {
            image.setImage(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
            image = imageRepository.insert(image);
        } catch (IOException exception) {
          throw new ImageAttachmentException("Error occurred during image attachment!");
        }

        return image.getId();
    }

    public Image getImage(String id) {
        return imageRepository.findById(id).get();
    }

}
