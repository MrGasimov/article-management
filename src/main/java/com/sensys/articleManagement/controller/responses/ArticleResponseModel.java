package com.sensys.articleManagement.controller.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ArticleResponseModel {
    private String title;
    private String description;
}
