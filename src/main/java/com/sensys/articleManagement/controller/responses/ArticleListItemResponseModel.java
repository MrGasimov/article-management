package com.sensys.articleManagement.controller.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleListItemResponseModel {
    private String title;
    private int imageCount;

}
