package com.sensys.articleManagement.controller;

import com.sensys.articleManagement.controller.requests.ArticleRequestModel;
import com.sensys.articleManagement.controller.responses.ArticleListItemResponseModel;
import com.sensys.articleManagement.controller.responses.ArticleResponseModel;
import com.sensys.articleManagement.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/article")
@RequiredArgsConstructor
public class ArticleManagementController {

    private final ArticleService articleService;

    @PostMapping("/new_article")
    public @ResponseBody ResponseEntity<ArticleResponseModel> createArticle(@Valid @RequestBody ArticleRequestModel articleRequestModel) {
        return ResponseEntity.ok().body(articleService.createArticle(articleRequestModel));
    }

    @GetMapping("/list_articles")
    public @ResponseBody ResponseEntity<List<ArticleListItemResponseModel>> listArticles() {
        return ResponseEntity.ok().body(articleService.listArticles());
    }

    @PostMapping("/add_image_to_article")
    public @ResponseBody ResponseEntity<ArticleResponseModel> addArticleImage(@RequestParam("file") MultipartFile file, String articleId) {
        var response = articleService.addImageToArticle(file, articleId);
        return ResponseEntity.ok(response);
    }
}
