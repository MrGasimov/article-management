package com.sensys.articleManagement.controller.requests;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
public class ArticleRequestModel {

    @NotBlank
    @Size(min = 3, max = 200, message = "title need to have between 10 and 200 characters")
    private String title;
    @NotBlank
    @Size(min = 3, max = 4000, message = "description need to have max 4000 characters")
    private String description;
    private List<String> attachedImages;

}
