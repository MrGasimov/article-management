package com.sensys.articleManagement.repository;

import com.sensys.articleManagement.repository.documents.Image;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends MongoRepository<Image, String> {

}
