package com.sensys.articleManagement.repository;

import com.sensys.articleManagement.repository.documents.ArticleItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ArticleRepository extends MongoRepository<ArticleItem, String> {

    @Override
    Optional<ArticleItem> findById(String id);

    List<ArticleItem> findArticleItemByTitle(String title);

    @Override
    ArticleItem insert(ArticleItem entity);

    List<ArticleItem> findAll();

    @Override
    ArticleItem save(ArticleItem entity);
}
