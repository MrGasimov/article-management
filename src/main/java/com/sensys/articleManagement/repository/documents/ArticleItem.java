package com.sensys.articleManagement.repository.documents;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document("articles")
public class ArticleItem {
    @Id
    private String id;
    private String title;
    private String description;
    private List<String> attachedImages;

}


